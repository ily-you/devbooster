const Encore = require('@symfony/webpack-encore');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackExcludeEmptyAssetsPlugin = require('html-webpack-exclude-empty-assets-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const RobotstxtPlugin = require('robotstxt-webpack-plugin');
const SitemapPlugin = require('sitemap-webpack-plugin').default;
const path = require('path');
const glob = require('glob');

// Pages and views detected.
const pages = glob.sync('./pages/**/*.html?(.twig)').map((viewFile) => {
    return {
        viewFile: viewFile,
        uriPath: viewFile.replace(/^(\.\/)?pages\//, '').replace(/\.twig$/, ''),
    };
});

class Debpack {
    constructor() {
        // Encore.reset();
        this._twigEnabled = true;
        this._faviconEnabled = true;
        this._faviconConfig = null;
        this._robotsTxtEnabled = true;
        this._sitemapEnabled = true;
        this._language = 'fr-FR';

        this._styles = new Map();
        this._scripts = new Map();

        this._config = null;

        this._packageJson = require('../package.json');
    }

    enableTwig() {
        this._twigEnabled = true;

        return this;
    }

    disableTwig() {
        this._twigEnabled = false;

        return this;
    }

    enableFavicon() {
        this._faviconEnabled = true;

        return this;
    }

    disableFavicon() {
        this._faviconEnabled = false;

        return this;
    }

    configFavicon(config) {
        this._faviconConfig = config;

        return this;
    }

    enableRobotsTxt() {
        this._robotsTxtEnabled = true;

        return this;
    }

    disableRobotsTxt() {
        this._robotsTxtEnabled = false;

        return this;
    }

    enableSitemap() {
        this._sitemapEnabled = true;

        return this;
    }

    disableSitemap() {
        this._sitemapEnabled = false;

        return this;
    }

    setLanguage(language) {
        this._language = language;
    }

    getWebpackConfig() {
        if (null === this._config) {
            this._initEncore();

            this._styles.forEach((src, name) => {
                Encore.addStyleEntry(name, src);
            });

            this._scripts.forEach((src, name) => {
                Encore.addEntry(name, src);
            });

            this._initAssets();
            this._initTwig();
            this._initFavicon();
            this._initRobotsTxt();
            this._initSitemap();

            this._config = Encore.getWebpackConfig();

            if (Encore.isDevServer()) {
                Object.assign(this._config.devServer, {
                    // Fallback to 404 page.
                    historyApiFallback: {
                        disableDotRule: true,
                        rewrites: [
                            { from: /./, to: '/404.html' },
                        ],
                    },
                });
            }
        }

        return this._config;
    }

    addStyle(name, src) {
        if (this._styles.has(name)) {
            throw new Error(`Duplicate name "${name}" passed to addStyle(): entries must be unique.`);
        }

        // also check for entries duplicates
        if (this._scripts.has(name)) {
            throw new Error(`The "${name}" passed to addStyle() conflicts with a name passed to addScript(). The entry names between addScript() and addStyle() must be unique.`);
        }

        this._styles.set(name, src);

        return this;
    }

    addScript(name, src) {
        if (this._scripts.has(name)) {
            throw new Error(`Duplicate name "${name}" passed to addScript(): entries must be unique.`);
        }

        // also check for entries duplicates
        if (this._styles.has(name)) {
            throw new Error(`The "${name}" passed to addScript() conflicts with a name passed to addStyle(). The entry names between addScript() and addStyle() must be unique.`);
        }

        this._scripts.set(name, src);

        return this;
    }

    /**
     * Init Webpack Encore
     *
     * @private
     */
    _initEncore() {
        Encore
            .setOutputPath('./dist')
            .setPublicPath('/')
            .setManifestKeyPrefix('')
            .enableSingleRuntimeChunk()
            .cleanupOutputBeforeBuild()
            .enableBuildNotifications()
            .enableSourceMaps(!Encore.isProduction())
            .enableVersioning(Encore.isProduction())

            .addAliases({
                '@build': path.resolve('.', 'build/'),
            })

            .enableSassLoader()
            .enableTypeScriptLoader()
        ;
    }

    _initAssets() {
        Encore
            .copyFiles({
                from: './assets',
                to: 'assets/[path][name].[ext]',
            })
        ;
    }

    /**
     * Register Twig as template engine.
     *
     * @private
     */
    _initTwig() {
        if (this._twigEnabled) {
            Encore
                .addLoader({
                    test: /\.twig$/,
                    use:  {
                        loader: 'twigjs-loader',
                        options: {
                            /**
                             * @param {object}  twigData        Data passed to the Twig.twig function
                             * @param {string}  twigData.id     Template id (relative path)
                             * @param {object}  twigData.tokens Parsed AST of a template
                             * @param {string}  dependencies    Code which requires related templates
                             * @param {boolean} isHot           Is Hot Module Replacement enabled
                             * @return {string}
                             */
                            renderTemplate: (twigData, dependencies, isHot) => {
                                const vars = Object.assign(this._getGlobalTwigVariables(), {
                                    current_path: twigData.id
                                        .replace(/\\/g, '/')
                                        .replace(/^pages\//, '')
                                        .replace(/\.html\.twig$/, '')
                                    ,
                                });

                                return `
                                    ${dependencies}
                                    const twig = require('@build/twig')(${JSON.stringify(vars)}).twig;
                                    const tpl = twig(${JSON.stringify(twigData)});
                                    module.exports = function(context) {
                                        return tpl.render(Object.assign({}, context, ${JSON.stringify(vars)}));
                                    };
                                `;
                            },
                        },
                    },
                })
                .addAliases({
                    '@templates': path.resolve('.', 'templates/'),
                    '@pages': path.resolve('.', 'pages/'),
                })
            ;
        }

        pages.forEach((page) => {
            Encore
                .addPlugin(new HtmlWebpackPlugin(Object.assign({}, {
                    minify: {
                        collapseWhitespace: Encore.isProduction(),
                        removeComments: Encore.isProduction(),
                        removeRedundantAttributes: Encore.isProduction(),
                        removeScriptTypeAttributes: Encore.isProduction(),
                        removeStyleLinkTypeAttributes: Encore.isProduction(),
                        useShortDoctype: true,
                    },
                }, {
                    template: page.viewFile,
                    filename: page.uriPath,
                })))
            ;
        });

        Encore
            .addPlugin(new HtmlWebpackExcludeEmptyAssetsPlugin())
        ;
    }

    /**
     * Register favicon optimization.
     *
     * @private
     */
    _initFavicon() {
        if (this._faviconEnabled) {
            if (typeof this._faviconConfig === 'string') {
                this._faviconConfig = {
                    logo: this._faviconConfig,
                    mode: 'webapp',
                    devMode: 'webapp',
                    favicons: {
                        lang: this._language,
                        appName: this._packageJson.name || null,
                        appDescription: this._packageJson.description || null,
                        developerName: this._packageJson.author.name || null,
                        developerURL: this._packageJson.author.url || null,
                        icons: {
                            android: true,
                            appleIcon: true,
                            appleStartup: false,
                            coast: true,
                            favicons: true,
                            firefox: true,
                            windows: true,
                            yandex: true,
                        },
                    },
                };
            }

            Encore
                .addPlugin(new FaviconsWebpackPlugin(this._faviconConfig))
            ;
        }
    }

    /**
     * Register robots.txt generator.
     *
     * @private
     */
    _initRobotsTxt() {
        if (this._robotsTxtEnabled) {
            Encore
                .addPlugin(new RobotstxtPlugin({
                    sitemap: this._getDeployHost() + '/sitemap.xml',
                    host: this._getDeployHost(),
                }))
            ;
        }
    }

    /**
     * Init sitemap
     *
     * @private
     */
    _initSitemap() {
        if (this._sitemapEnabled) {
            Encore
                .addPlugin(new SitemapPlugin(this._getDeployHost(), pages.filter((page) => {
                    return '404.html' !== page.uriPath;
                }).map((page) => {
                    return {
                        path: page.uriPath,
                        lastMod: new Date().toISOString(),
                        priority: 0.9,
                        changeFreq: 'monthly',
                    };
                }), {
                    skipGzip: true,
                }))
            ;
        }
    }

    /**
     * @returns {string}
     * @private
     */
    _getDeployHost() {
        // Full qualify host.
        let deployHost = process.env.DEPLOY_HOST || null;
        if (null === deployHost) {
            if (Encore.isDevServer()) {
                const config = Encore.getWebpackConfig();

                deployHost = config.devServer.publicPath.replace(/\/$/, '');
            } else {
                deployHost = 'http://localhost';
            }
        }

        return deployHost;
    }

    _getGlobalTwigVariables() {
        return {
            host: this._getDeployHost(),
            language: this._language,
        };
    }
}

module.exports = new Debpack();
