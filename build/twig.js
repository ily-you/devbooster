const Twig = require('twig');

let _global = null;

Twig.extendFunction('asset', (asset) => {
    return '/' + asset.replace(/\/$/, '').replace(/^\//, '');
});

Twig.extendFunction('path', (name) => {
    return '/' + name.replace(/^\//, '').replace(/\.twig$/, '').replace(/\.html$/, '') + '.html';
});

module.exports = (global = null) => {
    if (null === _global) {
        _global = global;
    }

    return Twig;
};
